<!DOCTYPE html><html lang="en-GB"><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><title>Static commenting | Blog | Tom Vincent</title><meta name="description" content="A review of static commenting solutions for static website generators"><meta name="viewport" content="width=device-width"><link rel="alternate" href="http://tlvince.com/feed" type="application/rss+xml" title="Essays on hacking, travel and self-improvement"><link rel="stylesheet" href="/assets/styles/main.css"></head><body><header class="header"><div class="content-wrap"><span class="title"><a href="/">Tom Vincent</a></span><p class="description">Essays on hacking, travel and self-improvement</p></div></header><div id="content"><div class="content-wrap"><article><header><h1 id="static-commenting"><a href="#static-commenting" class="anchor"></a>Static commenting</h1><time datetime="2012-10-19T09:52:22+01:00" title="Fri Oct 19 2012 09:52:22 GMT+0100">19<span class="ord">th</span> October,&nbsp;2012</time></header><section id="content"><p id="static-website-generators"><a href="#static-website-generators" class="anchor"></a>Static website generators typically rely on third-parties to provide commenting
services. This post introduces <em>static commenting</em> as an alternative and
reviews existing&nbsp;solutions.</p>
<h2 id="background"><a href="#background" class="anchor"></a>Background</h2>
<p id="comments-are-an"><a href="#comments-are-an" class="anchor"></a>Comments are an oft-neglected facet of static website generators. Typically,
they are outsourced to third-parties (Disqus <a href="http://alternativeto.net/software/disqus/">et al</a>., social networks
and news websites) or are simply <a href="http://mattgemmell.com/2011/11/29/comments-off/">switched&nbsp;off</a>.</p>
<p id="whilst-the-pros"><a href="#whilst-the-pros" class="anchor"></a>Whilst the <a href="http://avc.blogs.com/a_vc/2008/05/three-reasons-t.html">pros</a> and <a href="http://www.jeremyscheff.com/2011/08/jekyll-and-other-static-site-generators-are-currently-harmful-to-the-free-open-source-software-movement/">cons</a> of the aforementioned are
well-documented, the middle-ground &#8212; a commenting system that doesn&#8217;t rely on
a third-party that&#8217;s suitable for static websites &#8212; is not. Lets call this
<em>static commenting</em> and explore its&nbsp;characteristics.</p>
<h2 id="static-commenting"><a href="#static-commenting" class="anchor"></a>Static&nbsp;commenting</h2>
<p id="what-constitutes-a"><a href="#what-constitutes-a" class="anchor"></a>What constitutes a good static commenting approach? Here are a few&nbsp;possibilities:</p>
<ul id="ul1"><a href="#ul1" class="anchor"></a>
<li>Adheres to the <em>progressive enhancement</em> strategy (doesn&#8217;t rely on&nbsp;JavaScript)</li>
<li>Uses a <em>plain-text</em> data&nbsp;store</li>
<li>Integrates well with existing static website&nbsp;generators</li>
</ul>
<p id="with-these-criteria"><a href="#with-these-criteria" class="anchor"></a>With these criteria in mind, the remainder of this post reviews existing
approaches to static&nbsp;commenting.</p>
<h3 id="jekyll-static-comments"><a href="#jekyll-static-comments" class="anchor"></a>Jekyll Static&nbsp;Comments</h3>
<p id="jekyllstaticcomments-source-on-github"><a href="#jekyllstaticcomments-source-on-github" class="anchor"></a><a href="http://theshed.hezmatt.org/jekyll-static-comments/">Jekyll::StaticComments</a> (<a href="https://github.com/mpalmer/jekyll-static-comments">source on
Github</a>), uses a <span class="caps">HTML</span> form and a <span class="caps">PHP</span> submission script
that emails comments to a given address. Comments are then converted to <span class="caps">YAML</span>
format using a mail user agent (<span class="caps">MUA</span>) hook and rendered in the site with example&nbsp;templates.</p>
<p id="whilst-not-entirely"><a href="#whilst-not-entirely" class="anchor"></a>Whilst not entirely static (a server running <span class="caps">PHP</span> is required), a nice
side-effect of this approach is spam is implicitly dealt with by existing email
anti-spam technique(s) (whether that&#8217;s your email provider&#8217;s spam protection or
<a href="https://en.m.wikipedia.org/wiki/Anti_spam">otherwise</a>).</p>
<p id="one-drawback-could"><a href="#one-drawback-could" class="anchor"></a>One drawback could be the level of manual intervention involved. Although the
author does not specify the exact details of the <span class="caps">MUA</span> hook, the process appears
to be only partially automated and favours&nbsp;hand-moderation.</p>
<p id="however-derivatives-that"><a href="#however-derivatives-that" class="anchor"></a>However, derivatives that provide further automation are easily achievable. <a href="http://hezmatt.org/~mpalmer/blog/2011/07/19/static-comments-in-jekyll.html#fnref:comment-antispam">As
the author mentions</a>, the <span class="caps">PHP</span> script could be modified to
automatically commit the comment to a <code>git</code> repository and, with <a href="https://github.com/mojombo/jekyll/wiki/Deployment">further
hooks</a>, rebuild the&nbsp;site.</p>
<p id="the-mua-hook"><a href="#the-mua-hook" class="anchor"></a>The <span class="caps">MUA</span> hook is central to the suggested workflow. A mail client that supports
hooks/external scripting (such as Mutt) is therefore a prerequisite for this
approach. The alternative (copy/pasting the email) is unlikely to be attractive
for web mail users or email clients less command-line&nbsp;orientated.</p>
<p id="that-said-the"><a href="#that-said-the" class="anchor"></a>That said, the suggested workflow is only one example; the script itself does
not force it and is flexible to accommodate other approaches. For example,
email rules could be set up that automatically forward the comment for&nbsp;deployment.</p>
<p id="alternatively-as-the"><a href="#alternatively-as-the" class="anchor"></a>Alternatively, as the author mentions (in comment of the blog post), email
could be removed entirely by having the <span class="caps">PHP</span> script dump each comment on a
server for the moderator to periodically download and review. Further still,
integration with <a href="https://en.m.wikipedia.org/wiki/Akismet">Akismet</a> could produce a Wordpress-like, fully automated&nbsp;system.</p>
<h3 id="emailed-comments"><a href="#emailed-comments" class="anchor"></a>Emailed&nbsp;comments</h3>
<p id="tomas-carnecky-takes"><a href="#tomas-carnecky-takes" class="anchor"></a><a href="https://blog.caurea.org/2012/03/31/this-blog-has-comments-again.html">Tomas Carnecky</a> takes Jekyll Static Comment&#8217;s approach one
further: removing the form and <span class="caps">PHP</span> components and asking commenters to email&nbsp;directly.</p>
<p id="linkage-between-page"><a href="#linkage-between-page" class="anchor"></a>Linkage between page and comment is provided using a <code>mailto:</code> link composed of
a <code>comment+</code> prefix (plus addressing) and the page identifier (typically its
path). The emails are then processed in the same way as Jekyll Static Comments;
using a <span class="caps">MUA</span>&nbsp;hook.</p>
<p id="this-rather-ingenious"><a href="#this-rather-ingenious" class="anchor"></a>This rather ingenious use of <code>mailto:</code> can easily be integrated in existing
templates and therefore is truly static; no plugins or server&nbsp;required.</p>
<p id="it-could-be"><a href="#it-could-be" class="anchor"></a>It could be argued that sending an email rather than writing in a form
disconnects the commenter from the website somewhat. Also, loading an email
client just to leave a short comment (for example) may deter some&nbsp;users.</p>
<p id="on-the-other"><a href="#on-the-other" class="anchor"></a>On the other hand, the user will already by well acquainted with the writing
environment provided by an email client (which are typically better suited than
a plain input box). Second, most modern browsers can associate <code>mailto:</code>
handlers with webmail clients, minimising loading time&nbsp;friction.</p>
<h3 id="the-rest"><a href="#the-rest" class="anchor"></a>The&nbsp;rest</h3>
<p id="other-approaches-that"><a href="#other-approaches-that" class="anchor"></a>Other approaches that don&#8217;t meet the criteria of static commenting though may
be of interest include <a href="http://pyblosxom.github.com/">PyBlosxom</a>; a well-matured Python blogging engine
that ships with a <a href="http://pyblosxom.github.com/1.5/plugins/comments.html">comment plugin</a> but sadly does not work
with static rendering, <a href="https://github.com/phusion/juvia">Juvia</a>; an open-source commenting service similar to
Disqus which depends on JavaScript and <a href="http://camendesign.com/code/nononsense_forum">NoNonsense Forum</a>; which,
as the name implies, is more a standalone forum but makes novel use of <span class="caps">RSS</span> as a
data&nbsp;store.</p>
<h2 id="conclusion"><a href="#conclusion" class="anchor"></a>Conclusion</h2>
<p id="this-post-set"><a href="#this-post-set" class="anchor"></a>This post set out to explore alternatives to third-party commenting services
that are suitable for static website&nbsp;generators.</p>
<p id="despite-best-efforts"><a href="#despite-best-efforts" class="anchor"></a>Despite best efforts, only two approaches were found; the first using a
traditional <span class="caps">PHP</span>-powered form, the second being email-based and using specially
purposed <code>mailto:</code> links.</p>
<p id="a-review-of"><a href="#a-review-of" class="anchor"></a>A review of both approaches was given and extensions to the former&nbsp;offered.</p>
</section></article></div></div><footer><div class="content-wrap"><section class="copy"><p>© 2015 <a href="/" title="Home">Tom Vincent</a></p><nav><ul><li><a href="/about">about</a></li><li><a href="/contact">contact</a></li><li><a href="//labs.tlvince.com">labs</a></li></ul></nav></section></div></footer></body></html>