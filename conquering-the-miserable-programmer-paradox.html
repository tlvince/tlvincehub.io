<!DOCTYPE html><html lang="en-GB"><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><title>Conquering the miserable programmer paradox | Blog | Tom Vincent</title><meta name="description" content="A productivity hack to overcome rote work"><meta name="viewport" content="width=device-width"><link rel="alternate" href="http://tlvince.com/feed" type="application/rss+xml" title="Essays on hacking, travel and self-improvement"><link rel="stylesheet" href="/assets/styles/main.css"></head><body><header class="header"><div class="content-wrap"><span class="title"><a href="/">Tom Vincent</a></span><p class="description">Essays on hacking, travel and self-improvement</p></div></header><div id="content"><div class="content-wrap"><article><header><h1 id="conquering-the-miserable-programmer-paradox"><a href="#conquering-the-miserable-programmer-paradox" class="anchor"></a>Conquering the miserable programmer paradox</h1><time datetime="2011-01-22T00:00:00+00:00" title="Sat Jan 22 2011 00:00:00 GMT+0000">22<span class="ord">nd</span> January,&nbsp;2011</time></header><section id="content"><p id="all-too-commonly"><a href="#all-too-commonly" class="anchor"></a>All too commonly, people work on things they despise. Programmers are no
different, especially since computers are particularly suited to repetitive
tasks. I experienced it recently but devised a challenge helped me overcome&nbsp;it.</p>
<h2 id="problem"><a href="#problem" class="anchor"></a>Problem</h2>
<p id="i-was-recently"><a href="#i-was-recently" class="anchor"></a>I was recently given a task so monotonous, it would make any programmer
<a href="http://blog.garlicsim.org/post/2840398276/the-miserable-programmer-paradox">miserable</a>. I was asked to check a series of stock codes to find which were
singularly-listed (i.e. listed on only one stock exchange). Presented with 160
rows of Excel data, I was expected to search Reuters for each stock code,
one-by-one <em>by&nbsp;hand</em>.</p>
<p id="having-experienced-similar"><a href="#having-experienced-similar" class="anchor"></a>Having experienced similar soul-destroying data entry work in previous
employment, with half a Computer Science degree under my belt, I took a
different route and wrote an algorithm to do the work for me. <a href="https://secure.wikimedia.org/wikipedia/en/wiki/Work_smart">Work smart, not&nbsp;hard</a>.</p>
<h2 id="solution"><a href="#solution" class="anchor"></a>Solution</h2>
<p id="i-took-the"><a href="#i-took-the" class="anchor"></a>I took the task and devised a way of making it interesting. I challenged myself
to write an algorithm, using full <a href="https://secure.wikimedia.org/wikipedia/en/wiki/Test-driven_development">test-driven development</a> (<span class="caps">TDD</span>)
principles, in a language I have little experience with (Python 3), all <strong>within
one&nbsp;hour</strong>.</p>
<p id="after-clarifying-the"><a href="#after-clarifying-the" class="anchor"></a>After clarifying the requirements, I outlined the core algorithm on paper. I&#8217;d
save time by manually copy/pasting the stock codes into a plain-text file and
reading in each to query against Reuters. Closer inspection of the Reuters page
showed a redirect occurred when the stock was singularly listed. I therefore
planned to listen for the 301/302 <span class="caps">HTTP</span> redirect code on each request and log
each triggering stock to a&nbsp;file.</p>
<h2 id="reflection"><a href="#reflection" class="anchor"></a>Reflection</h2>
<h3 id="on-productivity"><a href="#on-productivity" class="anchor"></a>On&nbsp;productivity</h3>
<p id="like-most-software"><a href="#like-most-software" class="anchor"></a>Like most software projects, mine was late: one hour turned into <em>three</em>. The
fabricated estimate did however do wonders for my concentration; <strong>time pressure
is an awesome productivity&nbsp;tool</strong>.</p>
<p id="it-may-have"><a href="#it-may-have" class="anchor"></a>It may have taken longer, but the large majority of those three hours were of
solid focus; &#8220;wired in&#8221; as it&#8217;s known in <a href="https://secure.wikimedia.org/wikipedia/en/wiki/The_social_network">The Social&nbsp;Network</a>.</p>
<h3 id="on-scheduling"><a href="#on-scheduling" class="anchor"></a>On&nbsp;scheduling</h3>
<p id="note-this-was"><a href="#note-this-was" class="anchor"></a><em>Note</em>, this was three hours of <em>elapsed time</em> (rather than sole keyboard
bashing). Why? Because &#8220;how do you account for interruptions, unpredictable
bugs [and] status meetings&#8230;&#8221;? <a href="http://www.joelonsoftware.com/items/2007/10/26.html">You can&#8217;t,&nbsp;really</a>.</p>
<p id="only-the-people"><a href="#only-the-people" class="anchor"></a>Only the people who actually write the code &#8212; the developers &#8212; can estimate
how long a task will take, <em>but</em> only by recording honest <a href="https://secure.wikimedia.org/wikipedia/en/wiki/Velocity_(software_methodology)">velocity</a> (the
division of the estimate by the actual time spent) do the developer&#8217;s estimates
hold any weight. Experience makes you a better&nbsp;estimator.</p>
<h3 id="on-python-3"><a href="#on-python-3" class="anchor"></a>On Python&nbsp;3</h3>
<p id="without-going-into"><a href="#without-going-into" class="anchor"></a>Without going into too much depth, Python 3&#8217;s library module for requesting web
pages &#8212; <code>urllib.request</code> &#8212; has it&#8217;s limitations. Since redirect responses
are <a href="http://docs.python.org/py3k/library/urllib.request.html?highlight=urllib#urllib.request.FancyURLopener">silently ignored</a>, I found a mature third-party module &#8212;
<code>httplib2</code> &#8212; to overcome&nbsp;this.</p>
<p id="despite-strong-praise"><a href="#despite-strong-praise" class="anchor"></a>Despite strong praise in <a href="http://diveintopython3.org/http-web-services.html#introducing-httplib2">Dive Into Python 3</a> (<span class="caps">DIP3</span>, my main and excellent
resource for learning Python 3), I ran into a few bugs that prevented me from
using it. I lost around 1.5 hours troubleshooting, settling instead to redesign
my algorithm to scrape the Reuters page, compromising increased complexity for
<code>urllib.request</code> support.</p>
<p id="python-3-is"><a href="#python-3-is" class="anchor"></a>Python 3 is a very clean and expressional language, but as the author of <span class="caps">DIP3</span>
<a href="http://www.reddit.com/r/IAmA/comments/f545e/i_am_a_fourtime_published_author_i_write_free/c1dcgsm">reminds us</a>, it&#8217;s still very much in active development, with a low
adoption rate (so far) over Python&nbsp;2.</p>
<h3 id="on-workflows"><a href="#on-workflows" class="anchor"></a>On&nbsp;workflows</h3>
<p id="having-a-customised"><a href="#having-a-customised" class="anchor"></a>Having a customised, pre-configured setup particularly helped when the pressure
was on. Everything was at hand &#8212; windows managed by <a href="http://xmonad.org/">xmonad</a>, complete editing
efficiency with <a href="http://www.vim.org/">vim</a>, rounded off with continuous integration testing using
<a href="http://pypi.python.org/pypi/nosier">nosier</a> &#8212; helping me concentrate on the task at&nbsp;hand.</p>
<p id="tdd-is-worthwhile"><a href="#tdd-is-worthwhile" class="anchor"></a><span class="caps">TDD</span> is worthwhile methodology, effectively capturing your thought process as
formal specifications. It gives you confidence your code works and is robust
enough to withstand&nbsp;changes.</p>
<p id="writing-tests"><a href="#writing-tests" class="anchor"></a>Writing tests &#8212; <em>good tests</em> &#8212; is a real art however. Your tests should
<strong>cover corner cases</strong> and be easily <strong>repetable</strong>, with new tests <strong>only</strong> being
written <strong>when existing code&nbsp;passes</strong>.</p>
<p id="in-reality-mine"><a href="#in-reality-mine" class="anchor"></a>In reality, mine were narrow and couldn&#8217;t provide anything more than a small
inkling of&nbsp;assurance.</p>
</section></article></div></div><footer><div class="content-wrap"><section class="copy"><p>© 2015 <a href="/" title="Home">Tom Vincent</a></p><nav><ul><li><a href="/about">about</a></li><li><a href="/contact">contact</a></li><li><a href="//labs.tlvince.com">labs</a></li></ul></nav></section></div></footer></body></html>