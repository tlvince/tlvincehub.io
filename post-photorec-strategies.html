<!DOCTYPE html><html lang="en-GB"><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><title>Post-PhotoRec Strategies | Blog | Tom Vincent</title><meta name="description" content="An overview of approaches to sort files recovered by PhotoRec"><meta name="viewport" content="width=device-width"><link rel="alternate" href="http://tlvince.com/feed" type="application/rss+xml" title="Essays on hacking, travel and self-improvement"><link rel="stylesheet" href="/assets/styles/main.css"></head><body><header class="header"><div class="content-wrap"><span class="title"><a href="/">Tom Vincent</a></span><p class="description">Essays on hacking, travel and self-improvement</p></div></header><div id="content"><div class="content-wrap"><article><header><h1 id="post-photorec-strategies"><a href="#post-photorec-strategies" class="anchor"></a>Post-PhotoRec Strategies</h1><time datetime="2012-12-20T00:00:00+00:00" title="Thu Dec 20 2012 00:00:00 GMT+0000">20<span class="ord">th</span> December,&nbsp;2012</time></header><section id="content"><p id="if-youve-ever"><a href="#if-youve-ever" class="anchor"></a>If you&#8217;ve ever been in the unfortunate situation where your hard disk fails
beyond recognition (<a href="http://unix.stackexchange.com/questions/33284/recovering-ext4-superblocks">like mine did</a>), then you&#8217;ve likely come across
a low-level file recovery tool called <a href="http://www.cgsecurity.org/wiki/PhotoRec">PhotoRec</a>.</p>
<p id="photorec-does-a"><a href="#photorec-does-a" class="anchor"></a>PhotoRec does a fantastic job of recovering files by matching byte headers with
signatures of known file formats. At the time of writing, it recognises over
<a href="http://www.cgsecurity.org/wiki/File_Formats_Recovered_By_PhotoRec">440 file formats</a>, which covers just about every format you&#8217;re likely
to encounter&nbsp;day-to-day.</p>
<p id="however-the-challenge"><a href="#however-the-challenge" class="anchor"></a>However, the challenge <em>after</em> using PhotoRec is what to do with its output; the
unavoidable result of the data carving technique it uses is that the underlying
directory tree and file names are lost. You are therefore left with a flat-level
tree containing thousands of seemingly nonsensical files with file names such as
<code>f1191548088.txt</code>&#8230; Not particularly&nbsp;useful.</p>
<p id="this-post-looks"><a href="#this-post-looks" class="anchor"></a>This post looks at a few approaches you can use to organise the recovered&nbsp;files.</p>
<h2 id="sorting-strategies"><a href="#sorting-strategies" class="anchor"></a>Sorting&nbsp;strategies</h2>
<p id="lets-look-at"><a href="#lets-look-at" class="anchor"></a>Lets look at a few strategies to sort through the&nbsp;mess:</p>
<ul id="ul1"><a href="#ul1" class="anchor"></a>
<li><a href="#sort-by-file-extension">Sort by file&nbsp;extension</a></li>
<li><a href="#hash-audit">Hash&nbsp;audit</a></li>
<li><a href="#remove-corrupt-files">Remove corrupt&nbsp;files</a></li>
<li><a href="#rename-using-metadata">Rename using&nbsp;metadata</a></li>
</ul>
<h3 id="sort-by-file-extension"><a href="#sort-by-file-extension" class="anchor"></a>Sort by file&nbsp;extension</h3>
<p id="photorecs-after-using"><a href="#photorecs-after-using" class="anchor"></a>PhotoRec&#8217;s <a href="http://www.cgsecurity.org/wiki/After_Using_PhotoRec#Sort_files_by_extension">After Using PhotoRec</a> wiki page lists a few methods to sort
files after using the tool. The mentioned Python script collates each file by
its file extension. Whilst by no means fully solving the problem, this method
can help in combination with other approaches. Although unlikely, this may also
be of use if the file system in use has a <a href="http://stackoverflow.com/a/466596">maximum files per directory
limit</a>, such as&nbsp;<span class="caps">FAT32</span>.</p>
<h3 id="hash-audit"><a href="#hash-audit" class="anchor"></a>Hash&nbsp;audit</h3>
<p id="hashdeep-a-program"><a href="#hashdeep-a-program" class="anchor"></a><a href="http://md5deep.sourceforge.net/">hashdeep</a>, a program that computes and matches hashsets, has an <em>audit</em>
function that can compare file hashes against a known set. If you have a
known-good backup, this can be an effective way to determine which files you
already have and then prune them from PhotoRec&#8217;s&nbsp;set.</p>
<h3 id="rename-using-metadata"><a href="#rename-using-metadata" class="anchor"></a>Rename using&nbsp;metadata</h3>
<p id="a-fortunate-side-effect"><a href="#a-fortunate-side-effect" class="anchor"></a>A fortunate side-effect of using binary formats is that metadata is often saved
alongside its content. Depending on the format, a number of tools can be used to
re-organise the recovered file without reliance on file&nbsp;names.</p>
<h4 id="photos"><a href="#photos" class="anchor"></a>Photos</h4>
<p id="in-the-case"><a href="#in-the-case" class="anchor"></a>In the case of photos, we can use the excellent <a href="http://www.sno.phy.queensu.ca/~phil/exiftool/">exiftool</a> to rebuild a
directory tree based based on their&nbsp;timestamp:</p>
<pre id="pre1"><a href="#pre1" class="anchor"></a><code class="lang-bash">exiftool -r <span class="string">'-FileName&lt;CreateDate'</span> <span class="operator">-d</span> %Y/%m/%Y%m%d_%H%M%S%%-c.%%e [files]
</code></pre>
<h4 id="music"><a href="#music" class="anchor"></a>Music</h4>
<p id="music-can-be"><a href="#music-can-be" class="anchor"></a>Music can be handled elegantly using <a href="https://musicbrainz.org/doc/MusicBrainz_Picard">MusicBrainz Picard</a>. For a given
audio file, it will use acoustic fingerprinting techniques to generate a hash of
said file and then query it against the MusicBrainz database to determine its&nbsp;contents.</p>
<p id="be-sure-to"><a href="#be-sure-to" class="anchor"></a>Be sure to read through Picard&#8217;s <a href="https://musicbrainz.org/doc/How_to_Tag_Files_With_Picard">how-to guide</a>, particularly the
clustering function, which greatly speeds up the querying process. Also, at the
time of writing, the latest release of Picard (v1.2) contains a memory leak
which causes it to hang when dealing with large datasets. Try running the
<a href="https://github.com/musicbrainz/picard">development version</a> (the issue is resolved in pull-requests
<a href="https://github.com/musicbrainz/picard/pull/143">#143</a> and <a href="https://github.com/musicbrainz/picard/pull/146">#146</a>) if you experience&nbsp;this.</p>
<p id="alternatively-many-cloud-based"><a href="#alternatively-many-cloud-based" class="anchor"></a>Alternatively, many cloud-based music platforms such as Google Play Music or
iTunes have a &#8220;scan and match&#8221; feature (using similar fingerprinting
technologies as Picard), which will provide high bitrate, fully-tagged versions
of recognised files available to stream or&nbsp;re-download.</p>
<h3 id="remove-corrupt-files"><a href="#remove-corrupt-files" class="anchor"></a>Remove corrupt&nbsp;files</h3>
<p id="unfortunately-there-isnt"><a href="#unfortunately-there-isnt" class="anchor"></a>Unfortunately, there isn&#8217;t a universal way of determining whether a file is
corrupt. However, depending on the importance of your recovered data, there are
a few approaches worth&nbsp;trying:</p>
<h4 id="photos"><a href="#photos" class="anchor"></a>Photos</h4>
<p id="the-python-imaging"><a href="#the-python-imaging" class="anchor"></a>The <a href="http://www.pythonware.com/products/pil/">Python Imaging Library</a> (<span class="caps">PIL</span>) contains a <a href="http://effbot.org/imagingbook/image.htm">verify method</a>
(search for &#8216;verify&#8217;) that should catch obvious corruptions. After installing
<span class="caps">PIL</span>, try running Denilson Sá&#8217;s <a href="https://bitbucket.org/denilsonsa/small_scripts/src/96af96e23bc1e19c9156412cdbb8eeba09e21cad/jpeg_corrupt.py">jpeg_corrupt</a>, which is a thin
command-line-based wrapper around <span class="caps">PIL</span>&#8217;s verify method; given a glob of input
paths, it prints the names of those <em>verify</em> determines as&nbsp;corrupt.</p>
<h4 id="music-videos"><a href="#music-videos" class="anchor"></a>Music/Videos</h4>
<p id="running-ffmpeg-without"><a href="#running-ffmpeg-without" class="anchor"></a>Running <a href="http://ffmpeg.org/">ffmpeg</a> without an output file parameter displays information about
the given file. If ffmpeg is unable to parse the file, it&#8217;ll spit out a warning,
which can be leveraged to filter and delete corrupt files,&nbsp;e.g.:</p>
<pre id="pre2"><a href="#pre2" class="anchor"></a><code class="lang-bash">ffmpeg -i <span class="string">"<span class="variable">$i</span>"</span> <span class="number">2</span>&gt;&amp;<span class="number">1</span> | grep -q <span class="string">'Invalid data found when processing input'</span> &amp;&amp; rm <span class="string">"<span class="variable">$i</span>"</span>
</code></pre>
</section></article></div></div><footer><div class="content-wrap"><section class="copy"><p>© 2015 <a href="/" title="Home">Tom Vincent</a></p><nav><ul><li><a href="/about">about</a></li><li><a href="/contact">contact</a></li><li><a href="//labs.tlvince.com">labs</a></li></ul></nav></section></div></footer></body></html>